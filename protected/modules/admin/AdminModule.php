<?php

class AdminModule extends CWebModule
{
	public $defaultController = 'Question';
	
	public function init()
	{
		// $this->setImport(array(
		// 	'admin.components.*',
		// ));
		//$this->layoutPath = Yii::getPathOfAlias('admin.views.layouts');
		$this->layout = '/layouts/main';
	}
}
