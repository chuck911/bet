<h3>修改赌题 #<?php echo $model->id; ?></h3>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>

<?php if ($model->answer): ?>
<div class="alert">
	答案已经公布: <?php echo $model->answer==1?'真':'假' ?>
</div>
<?php else: ?>
<h3>公布答案 #<?php echo $model->id; ?></h3>

<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'id'=>'answer-form',
	'enableAjaxValidation'=>false,
	'action'=>array('/admin/question/confirmAnswer','id'=>$model->id),
)); ?>

	<?php echo $form->errorSummary($model); ?>

	<?php if(!$model->isNewRecord)echo $form->radioButtonListRow($model,'answer',array('0'=>'未公布','1'=>'真','2'=>'假')); ?>

	<div class="form-actions">
		<?php $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType'=>'submit',
			'type'=>'primary',
			'label'=>'公布答案',
		)); ?>
	</div>

<?php $this->endWidget(); ?>
<?php endif ?>