<h3>赌题管理</h3>

<?php $this->widget('bootstrap.widgets.TbGridView',array(
	'id'=>'question-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		array('name'=>'id','htmlOptions'=>array('style'=>'width:30px;')),
		'content',
		array(
			'name'=>'answer',
			'value'=>'$data->answer==0?"未公布":($data->answer==1?"真":"假")',
			'filter'=>false,
    ),
		array(
			'class'=>'bootstrap.widgets.TbButtonColumn',
			'buttons'=>array('view'=>array('visible'=>'false')),
		),
	),
)); ?>
