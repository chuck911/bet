<?php

class BetForm extends CFormModel
{
	public $guess;
	public $point;

	public function rules()
	{
		return array(
			array('guess,point', 'required'),
			array('point','numerical', 'integerOnly'=>true,'min'=>1),
			array('point','checkPoint')
		);
	}

	public function attributeLabels()
	{
		return array(
			'guess'=>'选择答案',
			'point'=>'下注点数',
		);
	}

	public function checkPoint($attribute,$params)
	{
		if(User::current()->point<$this->point)
			$this->addError('point','你的积分不够.');
	}
}