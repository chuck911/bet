<?php

/**
 * This is the model class for table "bet".
 *
 * The followings are the available columns in table 'bet':
 * @property integer $id
 * @property integer $questionID
 * @property integer $trueUserID
 * @property integer $falseUserID
 * @property integer $point
 */
class Bet extends CActiveRecord
{
	const tax = 2;

	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	public function tableName()
	{
		return 'bet';
	}

	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('questionID, point', 'required'),
			array('questionID, trueUserID, falseUserID, point', 'numerical', 'integerOnly'=>true,'min'=>1),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, questionID, trueUserID, falseUserID, point', 'safe', 'on'=>'search'),
		);
	}

	public function relations()
	{
		return array(
			'trueUser'=>array(self::BELONGS_TO,'User','trueUserID'),
			'falseUser'=>array(self::BELONGS_TO,'User','falseUserID'),
			'question'=>array(self::BELONGS_TO,'Question','questionID'),
		);
	}

	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'questionID' => 'Question',
			'trueUserID' => 'True User',
			'falseUserID' => 'False User',
			'point' => 'Point',
		);
	}
}