<?php

/**
 * This is the model class for table "question".
 *
 * The followings are the available columns in table 'question':
 * @property integer $id
 * @property string $content
 * @property integer $answer
 */
class Question extends CActiveRecord
{
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	public function tableName()
	{
		return 'question';
	}

	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('content', 'required'),
			array('answer', 'numerical', 'integerOnly'=>true),
			array('content', 'length', 'max'=>255),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, content, answer', 'safe', 'on'=>'search'),
		);
	}

	public function relations()
	{
		return array(
			'bets'=>array(self::HAS_MANY,'Bet','questionID'),
		);
	}

	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'content' => '内容',
			'answer' => '答案',
		);
	}

	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('content',$this->content,true);
		$criteria->compare('answer',$this->answer);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	public function publishResult($answer)
	{
		$answer = intval($answer);
		foreach ($this->bets as $bet) {
			if(!$bet->trueUserID && $bet->falseUserID){
				$bet->falseUser->earnPoint($bet->point);
				continue;
			}elseif($bet->trueUserID && !$bet->falseUserID){
				$bet->trueUser->earnPoint($bet->point);
				continue;
			}

			if($answer===1&&$bet->trueUserID){
				$bet->trueUser->earnPoint($bet->point*2-Bet::tax);
				// $bet->falseUser->losePoint($bet->point);
			}elseif($answer===2&&$bet->falseUserID){
				// $bet->trueUser->losePoint($bet->point);
				$bet->falseUser->earnPoint($bet->point*2-Bet::tax);
			}
		}
	}
}