<?php

class BetController extends Controller
{
	public $layout = 'bet';

	public function filters()
	{
		return array('rights');
	}

	public function allowedActions()
	{
		return '';
	}

	public function actionIndex()
	{
		$this->render('index');
	}

	public function actionQuestion($id)
	{
		$question = Utils::getOr404('Question',$id);
		$betForm = new BetForm;
		if(isset($_POST['BetForm'])){
			$this->newBet($question,$betForm);
		}
		$this->render('question',compact('question','betForm'));
	}

	private function newBet($question,$betForm)
	{
		$betForm->attributes=$_POST['BetForm'];
		if($betForm->validate()){
			$bet = new Bet;
			$bet->questionID = $question->id;
			$bet->point = $betForm->point;
			if($betForm->guess==1){
				$bet->trueUserID = Yii::app()->user->id;
			}elseif($betForm->guess==2){
				$bet->falseUserID = Yii::app()->user->id;
			}
			if($bet->save()){
				User::current()->losePoint($bet->point);
				Yii::app()->user->setFlash('success', '新下注成功，你花费了'.$bet->point.'个积分');
			}
			$this->refresh();
		}
	}

	public function actionRebet($id)
	{
		$bet = Utils::getOr404('Bet',$id);
		$guess = Utils::getParam('guess');
		if($guess==1 && !$bet->trueUserID){
			$bet->trueUserID = Yii::app()->user->id;
		}elseif($guess==2 && !$bet->falseUserID){
			$bet->falseUserID = Yii::app()->user->id;
		}else{
			throw new CHttpException(400,'非法请求');
		}
		if($bet->save()){
			User::current()->losePoint($bet->point);
			Yii::app()->user->setFlash('success', '对赌成功，你花费了'.$bet->point.'个积分');
		}
		$this->redirect(array('/bet/question','id'=>$bet->questionID));
	}

	public function actionMine()
	{
		$bets = User::current()->bets;
		$this->render('mine',compact('bets'));
	}
}