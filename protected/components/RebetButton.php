<?php
class RebetButton extends CWidget
{
	public $guess = true;
	public $bet;
	public function run()
	{
		$bet = $this->bet;
		$canAfford = User::current()->canAfford($bet->point);
		$isOver = !!$bet->question->answer;
		
		if($this->guess===true) {
			$self = $bet->falseUserID==Yii::app()->user->id;
			if ($bet->trueUserID)
				echo $bet->trueUser->name;
			elseif($isOver)
				echo '无人对赌';
			elseif(!$self && $canAfford) 
				echo CHtml::link('对赌',array('bet/rebet','id'=>$bet->id,'guess'=>1),array('class'=>'btn'));
			elseif(!$canAfford && !$self)
				echo '你的积分不够';
			else
				echo '暂时无人对赌';
		}elseif ($this->guess===false) {
			$self = $bet->trueUserID==Yii::app()->user->id;
			if ($bet->falseUserID)
				echo $bet->falseUser->name;
			elseif($isOver)
				echo '无人对赌';
			elseif(!$self && $canAfford) 
				echo CHtml::link('对赌',array('bet/rebet','id'=>$bet->id,'guess'=>2),array('class'=>'btn'));
			elseif(!$canAfford && !$self)
				echo '你的积分不够';
			else
				echo '暂时无人对赌';
		}
	}
}