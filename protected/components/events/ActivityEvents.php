<?php
require_once __DIR__.'/IEvents.php';

class ActivityEvents implements IEvents
{
	protected function getListeners()
	{
		return array(
			'FreeNote.created'=>'freeNoteCreated',
			// 'SquareShare.created'=>'squareShareCreated',
		);
	}
	
	public function addListeners()
	{
		foreach ($this->getListeners() as $event => $handler) {
			if(is_string($handler))
				Yii::app()->event->on($event,array($this,$handler));
			elseif(is_array($handler))
				Yii::app()->event->on($event,$handler);
		}
	}

	public function freeNoteCreated($freeNote)
	{
		$data = array(
			'freeNote'=>array(
				'content'=>$freeNote->content,
				'id'=>$freeNote->id,
			),
		);
		return Activity::send(ActivityType::FREENOTE,Yii::app()->user->id,$data);
	}

	public static function userData($user)
	{
		return array(
			'id'=>$user->id,
			'name'=>$user->name,
			'avatar'=>$user->avatar,
		);
	}
}
