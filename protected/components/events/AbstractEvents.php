<?php

class AbstractEvents {
	protected function getListeners()
	{
		return array();
	}
	
	public function addListeners()
	{
		foreach ($this->getListeners() as $event => $handler) {
			if(is_string($handler))
				Yii::app()->event->on($event,array($this,$handler));
			elseif(is_array($handler))
				Yii::app()->event->on($event,$handler);
		}
	}
}