##独立事件系统
.   
__该系统是一个非必要而且可能不完善的系统，并不强求使用__

事件系统即实现观察者模式的机制。Yii本身是有事件机制的，Yii的最底层类CComponent就有raiseEvent和attachEventHandler方法。那为什么还需要另一个事件系统呢。  
Yii中，绑定事件处理函数的过程（attachEventHandler）是在对象中完成的，它的事件处理机制也将随着对象的销毁而失效。而有些情况，我们需要一个全局的事件系统。

###目前代码存在的问题

我们使用MVC、分模块、封装widget，除了为复用，还为了实现[“关注点分离”](http://zh.wikipedia.org/wiki/%E5%85%B3%E6%B3%A8%E7%82%B9%E5%88%86%E7%A6%BB)，让你写代码的时候只专注解决一个问题。然而，随着系统复杂性的增加，我们发现上述那些方法还不够。

假设我要完成“评论日记”这个动作，我除了存储评论内容和与主题的关联外，可能还要 _发送提醒 , 发送广场动态 , 记录用户活跃度 , 或许还要记录 IP？_，而完成其他功能时，仍然需要反复执行类似的操作

这意味着我们虽然已经把程序按照功能进行了分解，却仍然有很多横切关注点被分散在了整套代码的各个地方（参见 [面向方面的程序设计](http://zh.wikipedia.org/wiki/%E9%9D%A2%E5%90%91%E4%BE%A7%E9%9D%A2%E7%9A%84%E7%A8%8B%E5%BA%8F%E8%AE%BE%E8%AE%A1)）

###解决方案

在PHP中引入标准的AOP显然是装X的行为，我们可以通过观察者模式基本达到“分离横切关注点”的效果。`Yii::app()->event`([EventEmitter](https://github.com/greenymora/qiwei.co/blob/master/protected/components/events/EventEmitter.php))是一个独立的事件管理类，它有两个最主要的方法:  
`on($event, $listener)`用来绑定事件  
`emit($event, $arguments)`用来抛出事件  
_类似的接口大家应该都经常使用_

我们可以通过[SEventBehavior](https://github.com/greenymora/qiwei.co/blob/master/protected/components/SEventBehavior.php)把原本只在model对象内的事件（created/updated/deleted）抛出来。举例说，用户完成“评论日记”动作时，将触发`DiaryComment.created`事件

这样，对于提醒、动态、活跃度等等的'横切关注点'，我们就可以在主程序外部分别做集中处理。我们再引入[eventsManager](https://github.com/greenymora/qiwei.co/blob/master/protected/components/events/SEventsManager.php)：
```
'eventsManager'=>array(
  'class'=>'application.components.events.SEventsManager',
    'listeners'=>array(
      'application.components.events.ActivityEvents',
      'notify.components.NotifyEvents',
    ),
),
```
SEventsManager所做的事情很简单，就是调用所有listener对象的`addListeners()`方法，在各对象的addListeners方法中，利用`Yii::app()->event`实现对事件的绑定。例如[NotifyEvents](https://github.com/greenymora/qiwei.co/blob/master/protected/modules/notify/components/NotifyEvents.php)对一些对象的created事件进行捕捉，集中进行提醒发送的操作。于是eventsManager就相当于实现了一个插件机制，可以挂接侧重任何关注点的类。 _当然有些事件也许是无法借助SEventBehavior抛出，可能需要手动抛出_