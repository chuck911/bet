<?php

class QuestionList extends CWidget
{
	public $items = array();

	public function init()
	{
		$questions = Question::model()->findAll();
		foreach ($questions as $question) {
			$label = $question->content;
			if($question->answer) $label.=' [已公布答案]';
			$this->items[]=array('label'=>$label,'url'=>array('/bet/question','id'=>$question->id));
		}
	}

	public function run()
	{
		echo Yii::app()->controller->widget('zii.widgets.CMenu',array(
			'items'=>$this->items,
			'htmlOptions'=>array('class'=>'nav nav-tabs nav-stacked'),
		),true);
	}
}