<?php

class SEventBehavior extends CActiveRecordBehavior
{
	public $onCreated = true;
	public $onUpdated = false;
	public $onDeleted = false;

	public function afterSave($event)
	{
		if($this->owner->isNewRecord && $this->onCreated) {
			$modelName = get_class($event->sender);
			$eventType = $modelName.'.created';
			Yii::app()->event->emit($eventType,array($event->sender,$eventType));
		}elseif($this->onUpdated){
			$modelName = get_class($event->sender);
			$eventType = $modelName.'.updated';
			Yii::app()->event->emit($eventType,array($event->sender));
		}
	}

	public function afterDelete($event)
	{
		if(!$this->onDeleted) return;
		$modelName = get_class($event->sender);
		Yii::app()->event->emit($modelName.'.deleted',array($event->sender));
	}
}
