<?php $this->layout='main' ?>
<div class="span9">
<?php if (count($bets)): ?>
<table class="table">
<thead>
<tr><th>题目</th><th>真</th><th>假</th><th>积分</th><th>结果</th><th>盈亏</th></tr>
</thead>
<tbody>
<?php foreach ($bets as $bet): ?>
<tr>
	<td><?php echo CHtml::link($bet->question->content,array('bet/question','id'=>$bet->questionID)) ?></td>
	<td><?php echo $bet->trueUserID?$bet->trueUser->name:'暂无对赌' ?></td>
	<td><?php echo $bet->falseUserID?$bet->falseUser->name:'暂无对赌' ?></td>
	<td><?php echo $bet->point ?></td>
	<td><?php
	if ($bet->question->answer){
		echo $bet->question->answer==1?'真':'假';
	}else{
		echo '尚未公布';
	}
?></td>
	<td><?php
	if($bet->trueUserID&&$bet->falseUserID){
		if (User::current()->hasWonBet($bet)){
			echo '+'.($bet->point*2-Bet::tax);
		}else{
			echo '-'.$bet->point;
		}
	}
	?></td>
</tr>
<?php endforeach ?>
</tbody>
</table>
<?php else: ?>
你还没有下任何赌注
<?php endif ?>

</div>