<h3><?php echo $question->content ?></h3>

<?php if ($question->answer): ?>
<div class="alert">答案已经公布，为<?php echo $question->answer===1?'真':'假' ?></div>
<?php else: ?>
<div class="well">

<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'id'=>'question-form',
	'enableAjaxValidation'=>false,
)); ?>
	<h4>新下注</h4>
	<?php echo $form->radioButtonList($betForm,'guess',array('1'=>'真','2'=>'假')) ?>
	<?php echo $form->error($betForm,'guess'); ?>
	<?php echo $form->textFieldRow($betForm,'point',array('maxlength'=>10)); ?>
	<div>
		<button type="submit" class="btn">下注</button>
	</div>
<?php $this->endWidget(); ?>

</div>
<?php endif ?>

<?php if (!count($question->bets)): ?>
<div>暂时无人下注</div>
<?php else: ?>
<table class="table">
<thead>
	<tr><th>真</th><th>积分</th><th>假</th></tr>
</thead>
<tbody>
	<?php foreach ($question->bets as $bet): ?>
	<tr>
		<td>
		<?php $this->widget('RebetButton',array('guess'=>true,'bet'=>$bet)) ?>
		</td>
		<td><?php echo $bet->point ?></td>
		<td>
		<?php $this->widget('RebetButton',array('guess'=>false,'bet'=>$bet)) ?>
		</td>
	</tr>
	<?php endforeach ?>
</tbody>
</table>
<?php endif ?>
