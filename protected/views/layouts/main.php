<?php /* @var $this Controller */ ?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
  <title><?php echo CHtml::encode($this->pageTitle); ?></title>
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link rel="stylesheet" href="<?php echo Yii::app()->baseUrl.'/css/app.css' ?>" type="text/css">
	<script src="<?php echo Yii::app()->baseUrl.'/js/jqClock.js' ?>" type="text/javascript" charset="utf-8"></script>
	<script src="<?php echo Yii::app()->baseUrl.'/js/app.js' ?>" type="text/javascript" charset="utf-8"></script>

</head>

<body>

<div class="navbar navbar-static-top">
	<div class="navbar-inner">
	<div class="container-fluid">
	<a class="brand span3" href="#"><?php echo Yii::app()->name ?></a>
	<div class="nav-collapse collapse">
	<?php
	$this->widget('zii.widgets.CMenu', array(
		'items'=>array(
			array('label'=>'首页', 'url'=>'/index.php'),
			array('label'=>'我的', 'url'=>array('/bet/mine')),
			array('label'=>'管理','url'=>array('/admin'),'visible'=>Yii::app()->user->checkAccess('Manager')),
			array('label'=>'权限设置','url'=>array('/rights'),'visible'=>Yii::app()->user->checkAccess('Admin')),
			
			// array('label'=>'关于', 'url'=>array('/site/page', 'view'=>'about')),
		),
		'htmlOptions'=>array('class'=>'nav'),
		'id'=>'main-menu'
		));
	?>
	<ul class="pull-right nav">
		<?php if(!Yii::app()->user->isGuest): ?>
		<li><a>积分：<?php echo User::current()->point ?></a></li>
		<li class="dropdown">
			<a class="dropdown-toggle" data-toggle="dropdown" href="#">
				<?php echo User::current()->name ?> <span class="caret"></span>
			</a>
			<ul class="dropdown-menu">
				<li><?php echo CHtml::link('设置','#') ?></li>
				<li><?php echo CHtml::link('我的主页','#') ?></li>
				<li class="divider"></li>
				<li><?php echo CHtml::link('退出',array('/site/logout')) ?></li>
			</ul>
		</li>
	<?php else: ?>
		<li><?php echo CHtml::link('登录',array('/site/login')) ?></li>
		<li><?php echo CHtml::link('注册',array('/site/register')) ?></li>
	<?php endif ?>
	</ul>
	</div><!--/.nav-collapse -->
	</div>
	</div>
</div>
<?php $this->widget('bootstrap.widgets.TbAlert', array(
	'block'=>true,
	'fade'=>true,
	'closeText'=>'&times;',
)); ?>
<div class="container-fluid">
	<div class="row-fluid">
		<?php echo $content ?>
	</div>
</div>

</body>
</html>
