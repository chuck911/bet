<h3>
<?php
$this->widget('zii.widgets.CMenu', array(
	'items'=>array(
		array('label'=>'登录', 'url'=>array('/site/login')),
		array('label'=>'注册', 'url'=>array('/site/register')),
	),
	'htmlOptions'=>array('class'=>'nav nav-pills'),
	));
?>
</h3>
<?php
$form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
    'id'=>'register-form',
		'enableAjaxValidation'=>false,
)); ?>
 
<?php echo $form->textFieldRow($model, 'username'); ?>
<?php echo $form->passwordFieldRow($model, 'password'); ?>
<?php echo $form->checkboxRow($model, 'rememberMe'); ?>

<div class="form-actions">
<?php $this->widget('bootstrap.widgets.TbButton', array('buttonType'=>'submit', 'type'=>'primary', 'label'=>'登录')); ?>
</div>
 
<?php $this->endWidget(); ?>

